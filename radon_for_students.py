import math

import matplotlib.pyplot as plt
import numpy as np
import numpy.fft as fft
import scipy.fftpack
import scipy.interpolate
import scipy.io
import scipy.misc
import scipy.ndimage
import scipy.ndimage.interpolation
import skimage.io


def plot_filter_sinogram(sinogram, ramp_filter, filtered_sinogram):
    """
    Plot sinogram, ramp filter and filtered sinogram.
    :param sinogram: The sinogram.
    :param ramp_filter: The ramp filter.
    :param filtered_sinogram: The filtered sinogram.
    """
    f, axarr = plt.subplots(2, 3)
    axarr[0, 0].set_title('sinogram')
    axarr[0, 1].set_title('ramp filter')
    axarr[0, 2].set_title('filtered sinogram')
    axarr[1, 0].set_title('sinogram first slice')
    axarr[1, 2].set_title('filtered sinogram first slice')
    if sinogram is not None:
        axarr[0, 0].imshow(sinogram)
        axarr[1, 0].plot(sinogram[0, :])
    if ramp_filter is not None:
        axarr[0, 1].plot(ramp_filter)
    if filtered_sinogram is not None:
        axarr[0, 2].imshow(filtered_sinogram)
        axarr[1, 2].plot(filtered_sinogram[0, :])


def plot_pst_with_interpolation(sinogram, sinogram_fft, sx, sy, sinogram_fft2d, fft2d_original):
    """
    Plot sinogram, sinogram with slice-wise fft, singoram fft points put into 2d fft and original 2d fft for comparison.
    :param sinogram: The sinogram.
    :param sinogram_fft: The sinogram with slice-wise fft.
    :param sx: The x-coordinates of the 2d fft sinogram.
    :param sy: The y-coordinates of the 2d fft sinogram.
    :param sinogram_fft2d: The interpolated 2d fft sinogram.
    :param fft2d_original: The 2d fft ofr the original image for comparison.
    """
    f, axarr = plt.subplots(2, 3)
    axarr[0, 0].set_title('sinogram')
    axarr[0, 1].set_title('sinogram 1D fft')
    axarr[1, 0].set_title('sinogram 2D fft')
    axarr[1, 1].set_title('interpolated 2D fft')
    axarr[1, 2].set_title('original 2D fft')
    if sinogram is not None:
        axarr[0, 0].imshow(sinogram)
    if sinogram_fft is not None:
        axarr[0, 1].imshow(np.abs(sinogram_fft), vmin=0, vmax=100)
    if sx is not None and sy is not None and sinogram_fft is not None:
        axarr[1, 0].scatter(sx.flatten(), sy.flatten(), c=np.abs(sinogram_fft.flatten()), marker='.', edgecolor='none', vmin=0, vmax=100)
    if sinogram_fft2d is not None:
        axarr[1, 1].imshow(np.abs(sinogram_fft2d), vmin=0, vmax=100)
    if fft2d_original is not None:
        axarr[1, 2].imshow(np.abs(fft2d_original), vmin=0, vmax=100)


def plot_results(backprojection, filtered_backprojection, pst_with_interpolation, image):
    """
    Plots the results of the three methods for reconstruction.
    :param backprojection: The results from backprojection.
    :param filtered_backprojection: The result from filtered backprojection.
    :param pst_with_interpolation: The result from the projection slice theorem with interpolating.
    :param image: The original image for comparison.
    """
    fig, axarr = plt.subplots(2, 2)
    axarr[0, 0].set_title('input')
    axarr[0, 1].set_title('backprojection')
    axarr[1, 0].set_title('filtered backprojection')
    axarr[1, 1].set_title('projection slice theorem with interpolation')
    if image is not None:
        axarr[0, 0].imshow(image, cmap='gray')
    if backprojection is not None:
        axarr[0, 1].imshow(backprojection, cmap='gray')
    if filtered_backprojection is not None:
        axarr[1, 0].imshow(filtered_backprojection, cmap='gray')
    if pst_with_interpolation is not None:
        axarr[1, 1].imshow(pst_with_interpolation, cmap='gray')


def load_image(filename):
    """
    Load the image at filename and return an np array.
    :param filename: The filename of the image to load.
    :return: The np.array of the loaded image.
    """
    image = skimage.io.imread(filename, as_gray=True)
    image = image.astype(np.float64)
    print(image.shape)
    print('image: min = ', np.min(image), ' max = ', np.max(image))

    return image


def pad_image(image):
    """
    Pad the image such that every rotation will fit into the padded image.
    :param image: The image to pad.
    :return: The padded image, the padding width.
    """
    # we assume image has same width and height
    padded_image_size = int(np.floor(image.shape[0] * np.sqrt(2)))
    # make sure padded image has odd dimensions
    if np.remainder(padded_image_size, 2) == 0:
        padded_image_size += 1

    padding_width = int((padded_image_size - image.shape[0]) / 2)

    # place the input image into a padded version, extending with zeros
    padded_image = np.zeros((padded_image_size, padded_image_size), dtype=np.float64)
    padded_image[padding_width:image.shape[0] + padding_width,
                 padding_width:image.shape[1] + padding_width] = image

    return padded_image, padding_width


def crop_image(image, crop_width):
    """
    Crop an image by the given width. Inverse of pad_image.
    :param image: The image to crop.
    :param crop_width: The crop width.
    :return: The cropped image.
    """
    if image is None:
        return None
    cropped_image = image[crop_width:image.shape[0] - crop_width,
                          crop_width:image.shape[1] - crop_width]

    return cropped_image


def calculate_sinogram(image, num_angles):
    """
    Calculate a sinogram from the given image for the given number of angles.
    :param image: The image.
    :param num_angles: The number of angles.
    :return: The sinogram.
    """
    # TODO: implement method
    image_size = image.shape[0]
    sinogram = np.zeros([num_angles, image_size], dtype=np.float64)

    # go over all angles to calculate one projection line of the sinogram
        # rotate image by angle, counterclockwise (scipy.ndimage.rotate)
        # compute projection by summing up over the 0th axis
        # put projection lines into columns (axis 0)
    for i in range(num_angles):
        rotation = scipy.ndimage.rotate(image, i, reshape=False)
        sinogram[i,:] = np.sum(rotation, axis=0, dtype=np.float64)
    return sinogram


def filter_sinogram(sinogram):
    """
    Filter the sinogram with a ramp filter.
    :param sinogram: The sinogram.
    :return: The filtered sinogram.
    """
    # TODO: implement method
    # create ramp filter in frequency domain (np.linspace)
    # normalize ramp filter (divide by sum)
    # go over all angles to receive one projection line of the sinogram
        # to apply discrete fft we need to shift to have zero in the center
        # multiply ramp filter in the frequency domain
        # inverse fft, extract real part, undo fftshift -> filtered line in the spatial domain
        # store filtered lines in output sinogram
    ramp_filter = np.linspace(0,360,360)
    n = len(ramp_filter)//2
    ramp_filter[:n],ramp_filter[n:] = ramp_filter[n:],ramp_filter[:n]
    ramp_filter[:n] = np.flip(ramp_filter[:n])
    ramp_filter = ramp_filter/sum(ramp_filter)
    filtered_sinogram = None
    plot_filter_sinogram(sinogram, ramp_filter, filtered_sinogram)

    return filtered_sinogram


def backproject(sinogram):
    """
    Backproject a (filtered) sinogram to create a reconstruction.
    :param sinogram: The (filtered) sinogram.
    :return: The reconstructed image.
    """
    # TODO: implement method
    # create result zero image
    # create image that contains a single sinogram line
    # go through every angle i in the filtered sinogram and calculate the filtered backprojection
        # put projection line into image that contains a single sinogram line, use np broadcasting, e.g., im[:, :] = sinogram[i, :]
        # rotate image according to angle (scipy.ndimage.rotate)
        # add to result image

    #!!!!! Source: https://github.com/csheaff/filt-back-proj/blob/master/py/filtbackproj.py
    #
    #We should try to rewrite this somehow and delete this comment afterwards. It's just for your information :)

    backprojection = np.zeros((sinogram.shape[1], sinogram.shape[1]))
    print("backprojection shape: ",backprojection.shape)
    print("sinogram shape: " , sinogram.shape)

    x = np.arange(sinogram.shape[1])-sinogram.shape[1]/2
    y = x.copy()
    X, Y = np.meshgrid(x, y)

    theta = np.linspace(1,sinogram.shape[0],sinogram.shape[0])
    theta = theta*np.pi/180
    numAngles = len(theta)

    for n in range(numAngles):
        Xrot = X*np.cos(theta[n])+Y*np.sin(theta[n])
        XrotCor = np.round(Xrot+sinogram.shape[1]/2)
        XrotCor = XrotCor.astype('int')
        projMatrix = np.zeros((sinogram.shape[1], sinogram.shape[1]))
        m0, m1 = np.where((XrotCor >= 0) & (XrotCor <= (sinogram.shape[1]-1)))
        s = sinogram[n,:]
        projMatrix[m0, m1] = s[XrotCor[m0, m1]]
        backprojection += projMatrix

    return backprojection


def calculate_sinogram_fft(sinogram):
    """
    Perform slice-wise 1d fft for a given sinogram.
    :param sinogram: The sinogram.
    :return: Slice-wise 1d fft of the sinogram.
    """
    # TODO: implement method
    # go over all angles and calculate 1D fft
    # hint: use fft.fftshift, fft.fft, and fft.ifftshift
    sinogram_fft = None

    return sinogram_fft


def get_sx_sy(sinogram_fft):
    """
    Return the sinogram x and y coordinates when putting it into the 2d fft image.
    :param sinogram_fft: The sinogram 1d fft.
    :return: The x coordinates of where to put back the sinogram_fft, the y coordinates of where to put back the sinogram_fft.
    """
    # TODO: implement method
    # x coordinates are: int(image_size / 2) + r * np.cos(a)
    # y coordinates are: int(image_size / 2) + r * np.sin(a)
    # hint: use linspace for angles and radii
    # hint: either use np.meshgrid, or iterate over all angles and radii
    sx = None
    sy = None

    return sx, sy


def interpolate_sinogram_fft_for_fft2d(sinogram_fft, sx, sy):
    """
    Interpolates the sinogram 1d fft from the given x and y coordinates into a 2d fft.
    :param sinogram_fft: The sinogram 1d fft.
    :param sx: The x coordinates of where to put back the sinogram_fft.
    :param sy: The y coordinates of where to put back the sinogram_fft.
    :return: The interpolated 2d fft.
    """
    # TODO: implement method
    # hint: use scipy.interpolate.griddata
    # hint: use np.meshgrid to create the points at which to interpolate data.
    # hint: use np.flatten() and np.reshape()
    fft2d = None

    return fft2d


def perform_fft2d(image):
    """
    Returns the 2d fft of the given image.
    :param image: The image.
    :return: The 2d fft of the image.
    """
    return None


def perform_ifft2d(image_fft2d):
    """
    Returns the 2d ifft of the given fourier image.
    :param image_fft2d: The fft2d image.
    :return: The original image.
    """
    # TODO: implement method
    # hint: use fft.fftshift, fft.ifft2, and fft.ifftshift
    return None


def calculate_pst_with_interpolation(sinogram, fft2d_original):
    """
    Calculate the projection slice theorem by generating a 1d fft for each line of the sinogram, putting it
    back into an image to get the 2d fft of the image, performing an interpolation, and a final inverse 2d fft.
    :param sinogram: The sinogram.
    :param fft2d_original: The original 2d fft of the input image. Only used for comparison.
    :return: The reconstruction.
    """
    # calculate slice-wise 1d fft
    sinogram_fft = calculate_sinogram_fft(sinogram)
    # get sinogram coordinates in 2d fft
    sx, sy = get_sx_sy(sinogram_fft)
    # put slice-wise 1d fft into 2d fft
    sinogram_fft2d = interpolate_sinogram_fft_for_fft2d(sinogram_fft, sx, sy)
    # reconstruct the original image with the inverse 2d fft
    recon = perform_ifft2d(sinogram_fft2d)

    plot_pst_with_interpolation(sinogram, sinogram_fft, sx, sy, sinogram_fft2d, fft2d_original)

    return recon


def main():
    """
    The main function.
    """
    filename = 'CTThoraxSlice257.png'
    num_angles = 180

    # load and pad image
    image = load_image(filename)
    padded_image, padding_width = pad_image(image)

    # calculate and save sinogram
    sinogram = calculate_sinogram(padded_image, num_angles)
    print('sinogram: min = ', np.min(sinogram), ' max = ', np.max(sinogram))

    # calculate and crop backprojection
    backprojection = backproject(sinogram)
    cropped_backprojection = crop_image(backprojection, padding_width)
    print('cropped_backprojection: min = ', np.min(cropped_backprojection), ' max = ', np.max(cropped_backprojection))

    # calculate and crop filtered backprojection
    filtered_sinogram = filter_sinogram(sinogram)
    filtered_backprojection = backproject(sinogram)
    cropped_filtered_backprojection = crop_image(filtered_backprojection, padding_width)
    print('cropped_filtered_backprojection: min = ', np.min(cropped_filtered_backprojection), ' max = ', np.max(cropped_filtered_backprojection))

    # calculate and crop with the projection slice theorem
    # 2D fft from original image, only used for comparison
    fft2d_original = perform_fft2d(image)
    pst_with_interpolation = calculate_pst_with_interpolation(sinogram, fft2d_original)
    cropped_pst_with_interpolation = crop_image(pst_with_interpolation, padding_width)
    print('cropped_pst: min = ', np.min(cropped_pst_with_interpolation), ' max = ', np.max(cropped_pst_with_interpolation))

    plot_results(cropped_backprojection, cropped_filtered_backprojection, cropped_pst_with_interpolation, image)

    plt.show()


if __name__ == '__main__':
    main()
